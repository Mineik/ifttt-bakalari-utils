from datetime import datetime
import requests
from time import sleep, time
from dateutil import parser
import traceback

from bakalari_py import Bakalari
from config import bakalariLogin, bakalariPassword, bakalariUrl, iftttToken, nextLessonNotificationEvent


def main():
    print("[STARTING] IFTTT bakalari notifier... [OK]")
    sendNotification("STARTED", "WATCHER")

    # Check the timetable every minute
    while True:
        try:
            b = Bakalari(bakalariLogin, bakalariPassword, bakalariUrl)
            rozvrh = b.rozvrh()
            tyden = SkolniTyden(rozvrh)
            if tyden.today():
                for hodina in tyden.today():
                    start = parser.parse(hodina.startTime)
                    now = datetime.now()
                    delta = start - now
                    if start > now and delta.seconds < 600:
                        sendNotificationIfEnoughTimeElapsed(hodina)

            print(f"[INFO] ({datetime.now()}) Sleeping")
            sleep(120)
        except KeyboardInterrupt:
            print("[STOPPED] IFTTT bakalari notifier")
            return
        except Exception as e:
            print(f"Exception occurred! error: {e}")
            traceback.print_exc()
            sleep(60)


class SkolniTyden():  # Trida pro zpracovavani XML z bakalaru
    def __init__(self, schedule_json):  # Argument je JSON s rozvrhem z Bakalaru
        self.rozvrh = []
        cisloDnu = 0

        self._make_list_subjects(schedule_json["Subjects"])
        self._make_list_teachers(schedule_json["Teachers"])
        self._make_list_rooms(schedule_json["Rooms"])

        # Pro kazdy den v rozvrhu se do self.rozvrh prida prazdne pole
        dny = schedule_json["Days"]
        for den in dny:
            hodiny = den["Atoms"]
            parsedTimetableTimes = {}
            for time in schedule_json["Hours"]:
                caption = time["Caption"]
                begintime = time["BeginTime"]
                endtime = time["EndTime"]
                parsedTimetableTimes[time["Id"]] = {"caption": caption, "begintime": begintime, "endtime": endtime}

            # Pro kazde pole (reprezentuje den ve skolnim rozvrhu) v self.rozvrh se do nej pridaji nazvy hodin, ktere v ten den jsou
            # for hodina in hodiny:
            #     if hodina.find("pr") != None:
            #         self.rozvrh[cisloDnu].append(hodina.find("pr").text)
            self.rozvrh.append(self._parseDay(hodiny, parsedTimetableTimes))

            cisloDnu += 1

    def _parseDay(self, day: list, parsedTimetableTimes: dict) -> list:
        hodiny = []
        for hodina in day:
            if hodina["Change"] is None:
                hodiny.append(self.Hodina(hodina, parsedTimetableTimes, self))
        return hodiny

    class Hodina:
        nazev: str
        ucebna: str
        ucitel: str
        startTime: int
        endTime: int

        def __init__(self, hodina_json, parsedTimetableTimes: dict, rozvrh_parent = None):

            self.nazev = rozvrh_parent.predmety[hodina_json["SubjectId"]].jmeno
            self.ucebna = rozvrh_parent.tridy[hodina_json["RoomId"]].zkratka
            self.ucitel = hodina_json.ucitele[hodina_json["TeacheId"]].zkratka

            self.startTime = parsedTimetableTimes[hodina_json["hourId"]]["begintime"]
            self.endTime = parsedTimetableTimes[hodina_json["hourId"]]["endtime"]

    def today(self):
        dayNumber = datetime.today().weekday()
        if dayNumber in [0, 1, 2, 3, 4]:
            return self.rozvrh[dayNumber]

    class Predmet:
        nazev: str
        zkratka: str

        def __init__(self, predmet_json):
            self.nazev = predmet_json["Name"]
            self.zkratka = predmet_json["Abbrev"]

    class Ucitel:
        jmeno: str
        zkratka: str

        def __init__(self, ucitel_json):
            self.jmeno = ucitel_json["Name"]
            self.zkratka = ucitel_json["Abbrev"]

    class Trida:
        jmeno: str
        zkratka: str

        def __init__(self, trida_json):
            self.jmeno = trida_json["Name"]
            self.zkratka = trida_json["Abbrev"]

    def _make_list_teachers(self, ucitele_json):
        ucitele = []

        for ucitel in ucitele_json:
            ucitele[ucitel["Id"]] = self.Ucitel(ucitel)

        self.ucitele = ucitele

    def _make_list_subjects(self, predmety_json):
        predmety = []

        for predmet in predmety_json:
            predmety[predmet["Id"]] = self.Predmet(predmet)

        self.predmety = predmety

    def _make_list_rooms(self, tridy_json):
        tridy = []

        for trida in tridy_json:
            tridy[trida["Id"]] = self.Trida(trida)

        self.tridy = tridy


def sendNotification(value1="", value2="", value3=""):
    r = requests.post(
        f"https://maker.ifttt.com/trigger/{nextLessonNotificationEvent}/with/key/{iftttToken}",
        json={"value1": value1, "value2": value2, "value3": value3})
    if r.status_code == 200:
        print(f'[OK] Notification sent for value1="{value1}", value2="{value2}", value3="{value3}".')
    else:
        print(f'[FAILED] Notification sent for value1="{value1}", value2="{value2}", value3="{value3}"!')
        print(f"Error. status_code: {r.status_code}, text: {r.text}")


lastNotificationSent = 0


def sendNotificationIfEnoughTimeElapsed(hodina: SkolniTyden.Hodina):
    global lastNotificationSent
    if lastNotificationSent + 1800 < time():
        sendNotification(hodina.nazev, hodina.ucebna)
        lastNotificationSent = time()


if __name__ == "__main__":
    main()
