import json

import requests
import datetime

# WIP BakalariApi class - NOT READY FOR USE
class BakalariApi:
    url: str
    token: str

    def __init__(self, bakalariUrl: str):
        self.url = bakalariUrl

    def login(self, username: str, password: str):
        r = requests.get(f"{self.url}/if/2/gethx/{username}")
        gethx = json.loads(r.text)

    def _send_request(self):
        pass


# Code from https://github.com/kokolem/bakalari-next-day/blob/master/bakalariNextDay.py
class Bakalari:  # Trida pro pristup k API bakalaru
    def __init__(self, login, password,
                 adresa):  # Argumenty jsou string s prihlasovacim jmenem, string s heslem a string s url adresou bakalaru skoly. Ze zadanych udaju se vypocita token a ulozi jako self.token

        # Adresa je potreba i v ostatnich metodach, heslo a prihlasovaci jmeno ne (staci token).
        self.adresa = adresa

        # Bakalari API V3 login
        login_result = requests.post(f'{self.adresa}/api/login', data={
            'client_id': 'ANDR',
            'grant_type': 'password',
            'username': login,
            'password': password
        })

        if login_result.ok:
            result_json = login_result.json()
            self.token = result_json["access_token"]
            self.refresh = result_json["refresh_token"]
        pass

    def rozvrh(self):
        # Ziskani rozvrhu
        scheduleStr = requests.get(f'{self.adresa}/api/3/timetable', params={
            'date': str(datetime.today())
        }, headers={
            "Authorization": f'Bearer {self.token}'
        })

        if not scheduleStr.ok:
            if(scheduleStr.status_code == 401):
                self._refresh_token()
                return self.rozvrh() # recursion because I just refreshed the token

        schedule = scheduleStr.json()
        return schedule

    def _refresh_token(self):
        result = requests.post(f'{self.adresa}/api/login', data={
            'client_id': 'ANDR',
            'grant_type': 'refresh_token',
            'refresh_token': self.refresh
        })

        if result.ok:
            result_json = result.json()
            self.token = result_json["access_token"]
            self.refresh = result_json["refresh_token"]
            return self.token
